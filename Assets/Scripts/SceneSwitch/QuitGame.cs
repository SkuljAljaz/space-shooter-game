﻿using UnityEngine;

public class QuitGame : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Laser")
        {
            Application.Quit();
        }
    }
}
