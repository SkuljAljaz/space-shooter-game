﻿using UnityEngine;

public class ScoreDEST : MonoBehaviour
{
    public GameObject player;

    void Start()
    {
        if(gameObject != null)
        {
            player = GameObject.Find("Player");
        }
    }
    void OnDestroy()
    {
        GivePoins(50);
    }

    void GivePoins(float points)
    {
        player.GetComponent<Interaction>().score += points;
    }
}
