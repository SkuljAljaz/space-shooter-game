﻿using UnityEngine;

public class Interaction : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth = 1;

    public int maxShield = 210;
    public int currentShield = 1;
    
    public Healthbar healthbar;
    public Shieldbar shieldbar;

    public float score = 0;

    public GameObject canvasDeath;

    void Start()
    {
        canvasDeath.SetActive(false);

        currentHealth = maxHealth;
        healthbar.SetMaxHealth(maxHealth);
        
        currentShield = maxShield;
        shieldbar.SetMaxShield(maxShield);
    }

    void Update()
    {   
        if(currentHealth <= 0)
        {
            canvasDeath.SetActive(true);
        }

    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.name == "Laser")
        {
            TakeDamage((int) other.gameObject.GetComponent<Laser>().damage);
        }
        else
        {
            TakeDamage(20);
        }

    }

    public void TakeDamage(int damage)
    {
        if(currentShield - damage < 0)
        {
            currentHealth -= Mathf.Abs(currentShield - damage);
            healthbar.SetHealth(currentHealth);
            
            if(currentShield > 0)
            {
                currentShield = 0;
                shieldbar.SetShield(currentShield);
            }
        }
        else
        {
            currentShield -= damage;
            shieldbar.SetShield(currentShield);
        }

    }

    public void updateStats()
    {
        healthbar.SetHealth(currentHealth);
        shieldbar.SetShield(currentShield);
    }
}
