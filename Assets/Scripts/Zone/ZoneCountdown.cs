﻿using UnityEngine;
using UnityEngine.UI;

public class ZoneCountdown : MonoBehaviour
{
    public float currentTime = 0f;
    public float startingTime = 10f;

    public Text countdown;

    public GameObject canvas;

    public GameObject player;

    bool inZone = false;

    void Start()
    {
        currentTime = startingTime;
        canvas.SetActive(false);
    }

    void Update()
    {
        if(inZone == true)
        {
            canvas.SetActive(true);
            currentTime -= 1 * Time.deltaTime;
            countdown.text = currentTime.ToString("0.0");

            if(currentTime < 0)
            {
                currentTime = 0;
                player.GetComponent<Interaction>().currentHealth = 0;
                canvas.SetActive(false);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        inZone = false;
        currentTime = 10f;
        canvas.SetActive(false);
    }

    void OnTriggerExit(Collider other) 
    {
        if(other.name == "Player")
        {
            inZone = true;
        }
    }
}
