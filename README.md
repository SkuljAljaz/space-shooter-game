##### 1. Spaceship
  - Movement
  - Shooting

##### 2. Enemies
  - Ships

##### 3. Obstacles
  - Meteors (they break into smaller pieces when destroyed)

##### 4. Powerups
  - Different pickups distributed around the level (health regeneration, shield regeneration, double current score, double damage and double shooting speed)

##### 5. Scoring
  - When destroying enemies, meteors and their pieces the player increases their score

##### 6. Background environment
  - Planets and star Skybox (textures: https://www.solarsystemscope.com/textures/)

##### 7. Enemy behaviors
  - Following player around
  - Stop when player is too close
  - Shooting at player when he is in range

##### Bonus:
  Game menu, some sounds, victory, death and out of bounds screens, every 3D object was modeled using Blender
