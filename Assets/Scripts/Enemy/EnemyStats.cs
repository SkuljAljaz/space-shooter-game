﻿using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    public float health = 200;
    public float score = 150;

    void Update()
    {
        if (health <= 0)
        {
            GameObject player = GameObject.Find("Player");
            player.GetComponent<Interaction>().score += score;
            Destroy(gameObject);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Laser")
        {
            health -= collision.gameObject.GetComponent<Laser>().damage;
        }
    }
}
