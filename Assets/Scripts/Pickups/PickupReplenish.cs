﻿using UnityEngine;

public class PickupReplenish : MonoBehaviour
{
    public int healthAmount = 50;

    void Update()
    {
        transform.Rotate(0, 90 * Time.deltaTime, 0 );
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            if(other.gameObject.GetComponent<Interaction>().currentHealth != other.gameObject.GetComponent<Interaction>().maxHealth)
            {
                other.gameObject.GetComponent<Interaction>().currentHealth = other.gameObject.GetComponent<Interaction>().maxHealth;
                other.gameObject.GetComponent<Interaction>().currentShield = other.gameObject.GetComponent<Interaction>().maxShield;
                other.gameObject.GetComponent<Interaction>().updateStats();
                Destroy(gameObject);
            }

            if(other.gameObject.GetComponent<Interaction>().currentShield != other.gameObject.GetComponent<Interaction>().maxShield)
            {
                other.gameObject.GetComponent<Interaction>().currentShield = other.gameObject.GetComponent<Interaction>().maxShield;
                other.gameObject.GetComponent<Interaction>().updateStats();
                Destroy(gameObject);
            }
            
        }
    }
}
