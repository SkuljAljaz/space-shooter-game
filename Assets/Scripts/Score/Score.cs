﻿using UnityEngine;

public class Score : MonoBehaviour
{
    public GameObject player;

    void Start()
    {
        if(gameObject != null)
        {
            player = GameObject.Find("Player");
        }
    }
    void OnDestroy()
    {
        GivePoins(50);
    }

    void GivePoins(float points)
    {   
        if(player != null)
        {
            player.GetComponent<Interaction>().score += points;
        }
    }
}
