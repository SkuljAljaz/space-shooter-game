﻿using UnityEngine;

public class PickupShield : MonoBehaviour
{
    public int shieldAmount = 50;

    void Update()
    {
        transform.Rotate(0, 90 * Time.deltaTime, 0 );
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            if(other.gameObject.GetComponent<Interaction>().currentShield != other.gameObject.GetComponent<Interaction>().maxShield)
            {
                if(other.gameObject.GetComponent<Interaction>().currentShield + shieldAmount > other.gameObject.GetComponent<Interaction>().maxShield)
                {
                    other.gameObject.GetComponent<Interaction>().currentShield = other.gameObject.GetComponent<Interaction>().maxShield;
                }
                else
                {
                    other.gameObject.GetComponent<Interaction>().currentShield += shieldAmount;
                }
                other.gameObject.GetComponent<Interaction>().updateStats();
                Destroy(gameObject);
            }
        }
    }
}
