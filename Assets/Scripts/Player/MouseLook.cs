﻿using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 100f;
    public Transform playerBody;
    public GameObject player;

    int health = 1;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        health = player.GetComponent<Interaction>().currentHealth;

        if(health > 0)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime; 

            playerBody.Rotate(Vector3.up * mouseX);
            playerBody.Rotate(Vector3.right * -mouseY);
        }

    }
}
