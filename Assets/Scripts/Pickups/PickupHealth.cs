﻿using UnityEngine;

public class PickupHealth : MonoBehaviour
{
    public int healthAmount = 50;

    void Update()
    {
        transform.Rotate(0, 90 * Time.deltaTime, 0 );
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            if(other.gameObject.GetComponent<Interaction>().currentHealth != other.gameObject.GetComponent<Interaction>().maxHealth)
            {
                if(other.gameObject.GetComponent<Interaction>().currentHealth + healthAmount >other.gameObject.GetComponent<Interaction>().maxHealth)
                {
                    other.gameObject.GetComponent<Interaction>().currentHealth = other.gameObject.GetComponent<Interaction>().maxHealth;
                }
                else
                {
                    other.gameObject.GetComponent<Interaction>().currentHealth += healthAmount;
                }
                other.gameObject.GetComponent<Interaction>().updateStats();
                Destroy(gameObject);
            }
           
        }
    }
}
