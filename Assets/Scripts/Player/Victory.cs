﻿using UnityEngine;

public class Victory : MonoBehaviour
{
    public GameObject canvasVictory;
    public GameObject player;

    int currentHealth;
    int maxHealth;

    void Start()
    {
        canvasVictory.SetActive(false);
        currentHealth = player.GetComponent<Interaction>().currentHealth;
        maxHealth = player.GetComponent<Interaction>().maxHealth;
    }

    void Update()
    {
        if(GameObject.FindWithTag("Meteor") == null)
        {
            canvasVictory.SetActive(true);
            currentHealth = maxHealth;
        }
    }
}
