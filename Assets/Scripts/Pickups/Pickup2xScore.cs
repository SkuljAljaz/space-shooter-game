﻿using UnityEngine;

public class Pickup2xScore : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(0, 90 * Time.deltaTime, 0 );
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            other.gameObject.GetComponent<Interaction>().score *= 2;
            Destroy(gameObject);          
        }
    }
}
