﻿using UnityEngine;

public class Pickup2xRPM : MonoBehaviour
{
    public float duration = 5f;
    public GameObject canvas;
    Shooting[] guns;
    bool RPM2x = false;

    private void Start()
    {
       canvas.SetActive(false); 
    }
    void Update()
    {
        transform.Rotate(0, 90 * Time.deltaTime, 0 );
        if(RPM2x == true)
        {
            duration -= Time.deltaTime;
            if(duration <= 0)
            {
                for(int i = 0; i < guns.Length; i++)
                {
                    guns[i].fireRate /= 2;
                }
                canvas.SetActive(false);
                Destroy(gameObject); 
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            canvas.SetActive(true);
            RPM2x = true;
            guns = other.gameObject.GetComponentsInChildren<Shooting>();
            for(int i = 0; i < guns.Length; i++)
            {
                guns[i].fireRate *= 2;
            }
            gameObject.GetComponent<Renderer>().enabled = false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
