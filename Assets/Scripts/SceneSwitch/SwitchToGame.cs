﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class SwitchToGame : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Laser")
        {
            SceneManager.LoadScene(1);
        }
    }
}
