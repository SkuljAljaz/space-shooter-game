﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public Rigidbody rb;
    public Transform target;
    public float rotationalDamp = 1f;
    public float movementSpeed = 10f;
    public float detDistance = 50f;
    public float raycastOffset = 2;

    bool inRangeOfPlayer = false;
    
    void Start()
    {
        GameObject player = GameObject.Find("Player");
        target = player.transform;
    }

    void Update()
    {
        Pathfinding();  
        if(inRangeOfPlayer == false){
            Move(); 
        }
    }

    void Move(){
        rb.MovePosition(transform.position + transform.forward * movementSpeed * Time.deltaTime);
    }

    void Turn(){
        Vector3 position = target.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationalDamp * Time.deltaTime);
    }

    void Pathfinding(){
        RaycastHit rayHit;
        
        if (Physics.Raycast(transform.position + transform.right * raycastOffset, transform.forward, out rayHit,  detDistance) || 
            Physics.Raycast(transform.position - transform.right * raycastOffset, transform.forward, out rayHit,  detDistance) ||
            Physics.Raycast(transform.position + transform.up * raycastOffset, transform.forward, out rayHit,  detDistance) ||
            Physics.Raycast(transform.position - transform.up * raycastOffset, transform.forward, out rayHit,  detDistance) )
        {
            if(rayHit.transform.tag == "Meteor")
            {
                Vector3 position = target.position + transform.position * 2;
                Quaternion rotation = Quaternion.LookRotation(position);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.1f * Time.deltaTime);
            }
            if (rayHit.transform.tag == "Enemy")
            {
                Turn();
            }

        }
        else
        {
            Turn();
        }  
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            inRangeOfPlayer = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            inRangeOfPlayer = false;
        }
    }

}
