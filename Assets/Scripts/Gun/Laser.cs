﻿using UnityEngine;

public class Laser : MonoBehaviour
{
    public float damage = 10f;
    public bool doubleDMG = false;
    public float delay = 3f;

    float countdown;

    void Start()
    {
        countdown = delay;
    }

    void Update()
    {
        countdown -= Time.deltaTime;
        if ( countdown <= 0f )
        {
            LaserVanish();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Target target = collision.transform.GetComponent<Target>();
        MeteorDEST target2 = collision.transform.GetComponent<MeteorDEST>();
        Interaction player = collision.transform.GetComponent<Interaction>();

        if (target != null)
        {
            target.TakeDamage(damage);
        }
        if (target2 != null)
        {
            target2.TakeDamage(damage);
        }
        if (player != null)
        {
            player.TakeDamage((int) damage);
        }
        LaserVanish();
    }

    void LaserVanish()
    {
        Destroy(gameObject);
    }

}
