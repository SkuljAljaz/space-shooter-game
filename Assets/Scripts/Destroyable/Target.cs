﻿using UnityEngine;

public class Target : MonoBehaviour
{
    public GameObject destroyedPrefab;
    public float health = 50f;
    bool alreadyInst = false;

    public void TakeDamage(float amount)
    {
        health -= amount;
        if(health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        if(alreadyInst == false)
        {
            GameObject meteor = Instantiate(destroyedPrefab, transform.position, transform.rotation);
            alreadyInst = true;
        }
        Destroy(gameObject);
    }

}
