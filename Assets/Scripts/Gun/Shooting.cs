﻿using UnityEngine;

public class Shooting : MonoBehaviour
{
    public float shootForce = 100f;
    public GameObject projectilePrefab;

    public float fireRate = 5f;
    float nextTimeToFire = 0;

    void Update()
    {
        if (Input.GetMouseButton(0) && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();    
        }
    }

    void Shoot()
    {
        Vector3 playerSpeed = gameObject.GetComponentInParent<Rigidbody>().velocity;
        GameObject laser = Instantiate(projectilePrefab, transform.position, transform.rotation);
        Rigidbody rb = laser.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * shootForce + playerSpeed, ForceMode.VelocityChange);
    }
}
