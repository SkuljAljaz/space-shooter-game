﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Reset : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKey(KeyCode.M))
        {
            SceneManager.LoadScene(0);
        }
        if (Input.GetKey(KeyCode.N))
        {
            SceneManager.LoadScene(1);
        }
    }
}
