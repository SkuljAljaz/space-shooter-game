﻿using UnityEngine;

public class Movement2 : MonoBehaviour
{
    public Rigidbody rb;

    public float speed = 25f;
    public float rotateLeft = 3f;
    public float rotateRight = 3f;

    public GameObject player;
    int health = 1;

    public float maxSpeedZ = 500f;
    public float maxSpeedX = 1000f;

    void FixedUpdate()
    {
        Vector3 move = new Vector3();
        Vector3 moveX = new Vector3();
        Vector3 moveZ = new Vector3();

        health = player.GetComponent<Interaction>().currentHealth;

        if(health > 0)
        {
            if (Input.GetKey(KeyCode.Q))
            {
                rb.transform.Rotate(0, 0, rotateLeft);
            }

            if (Input.GetKey(KeyCode.E))
            {
                rb.transform.Rotate(0, 0, -rotateLeft);
            }


            if (Input.GetKey(KeyCode.Space))
            {
                move = move + transform.up * speed;
            }

            if (Input.GetKey(KeyCode.LeftShift))
            {
                move = move - transform.up * speed;
            }


            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
            {
                float z = Input.GetAxis("Vertical");
                move = move + transform.forward * speed * z;
            }

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            {
                float x = Input.GetAxis("Horizontal");
                move = move + transform.right * speed * x;
            }
        }

        if (rb.velocity.z < maxSpeedZ)
        {
            move = move + moveZ;
        }

        if (rb.velocity.x < maxSpeedX)
        {
            move = move + moveX;
        }

        rb.MovePosition(transform.position + move * Time.deltaTime);
       
    }
}
