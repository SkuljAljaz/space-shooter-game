﻿using UnityEngine;

public class Pickup2xDamage : MonoBehaviour
{
    public GameObject canvas;

    public float duration = 5f;
    bool DMG2x = false;

    GameObject[] laserPrefab;
    Laser[] projectiles;


    void Start()
    {
       canvas.SetActive(false); 
    }

    void Update()
    {
        transform.Rotate(0, 90 * Time.deltaTime, 0 );
        if(DMG2x == true)
        {
            if(GameObject.FindWithTag("Laser"))
            {
                projectiles = GameObject.FindWithTag("Laser").GetComponents<Laser>();
                doubleDamage(projectiles);
            }
            duration -= Time.deltaTime;
            if(duration <= 0)
            {
                canvas.SetActive(false);
                Destroy(gameObject); 
            }
        }
    }

    void doubleDamage(Laser[] projectiles)
    {
        for(int i = 0; i < projectiles.Length; i++)
        {
            if(projectiles[i].doubleDMG == false)
            {
                projectiles[i].damage *= 2;
                projectiles[i].doubleDMG = true;
            }
            
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            DMG2x = true;
            canvas.SetActive(true);
            gameObject.GetComponent<Renderer>().enabled = false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
