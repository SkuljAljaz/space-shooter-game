﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    public Text scoreText;
    public GameObject player; 
    
    void Update()
    {
        scoreText.text = player.GetComponent<Interaction>().score.ToString("0");
    }
}
