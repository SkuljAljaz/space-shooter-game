﻿using UnityEngine;
using UnityEngine.UI;

public class Shieldbar : MonoBehaviour
{
    public Slider sliderShield;

    public void SetMaxShield(int shield)
    {
        sliderShield.maxValue = shield;
        sliderShield.value = shield;
    }

    public void SetShield(int shield)
    {
        sliderShield.value = shield;
    }
    
}
