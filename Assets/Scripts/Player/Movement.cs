﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    public Rigidbody controller;
    public float forwardSpeed = 250f;
    public float sideSpeed = 500f;
    public float stopingForce = 10f;
    public float maxSpeedZ = 500f;
    public float maxSpeedX = 1000f;

    public float rotateLeft = 3f;
    public float rotateRight = 3f;

    public GameObject player;

    int health = 1;

    void FixedUpdate()
    {
        Vector3 move = new Vector3();
        Vector3 moveX = new Vector3();
        Vector3 moveZ = new Vector3();
        bool hasInput = false;

        health = player.GetComponent<Interaction>().currentHealth;

        if(health > 0)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                hasInput = true;
                moveZ = transform.up * sideSpeed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.LeftShift))
            {
                hasInput = true;
                moveZ = -transform.up * sideSpeed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.Q))
            {
                controller.transform.Rotate(0, 0, rotateLeft);
            }

            if (Input.GetKey(KeyCode.E))
            {
                controller.transform.Rotate(0, 0, -rotateLeft);
            }


            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
            {
                hasInput = true;
                float z = Input.GetAxis("Vertical");
                moveZ = transform.forward * z * forwardSpeed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            {
                hasInput = true;
                float x = Input.GetAxis("Horizontal");
                moveX = transform.right * x * sideSpeed * Time.deltaTime;
            }
        }

        if (hasInput == false)
        {
            controller.AddForce(controller.velocity.normalized * Time.deltaTime * -stopingForce,ForceMode.VelocityChange);
        }
        else {

            if (controller.velocity.z < maxSpeedZ)
            {
                move = move + moveZ;
            }

            if (controller.velocity.x < maxSpeedX)
            {
                move = move + moveX;
            }

            controller.AddForce(move, ForceMode.Acceleration);
        }

    }
}
