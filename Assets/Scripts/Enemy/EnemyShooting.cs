﻿using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public float shootForce = 100f;
    public GameObject projectilePrefab;

    public float shootDelay = 0.3f;
    float countdown;

    bool playerInSight = false;

    void Start()
    {
        countdown = shootDelay;
    }

    void Update()
    {
        if(playerInSight == true)
        {
            countdown -= Time.deltaTime;
            if (countdown <= 0f)
            {
                Shoot();
                countdown = shootDelay;
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Player")
        {
            playerInSight = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Player")
        {
            playerInSight = false;
        }
    }

    void Shoot()
    {
        Vector3 enemySpeed = gameObject.GetComponentInParent<Rigidbody>().velocity;
        GameObject laser = Instantiate(projectilePrefab, transform.position, transform.rotation);
        Rigidbody rb = laser.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * shootForce + enemySpeed, ForceMode.VelocityChange);
    }
}
